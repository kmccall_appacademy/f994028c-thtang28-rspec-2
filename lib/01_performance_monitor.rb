def measure(num=1, &prc)
  start_time = Time.now
  num.times{prc.call}
  stop_time = Time.now
  (stop_time - start_time)/num
end
